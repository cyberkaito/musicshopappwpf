set ansi_padding on
go
set quoted_identifier on 
go
set ansi_nulls on
go

create database [MusicShop_Database]
go

use[MusicShop_Database]
go

create table [dbo].[Roles]
(
	[ID_Role] [int] not null identity(1,1),
	[Name_Role] [varchar] (50) not null
	constraint [PK_Role] primary key clustered ([ID_Role] ASC) on [PRIMARY],
	constraint [UQ_Name_Role] unique ([Name_Role])
)
go

create table [dbo].[Delivery_Kind]
(
	[ID_Delivery_Kind] [int] not null identity(1,1),
	[Name_Delivery_Kind] [varchar] (50) not null
	constraint [PK_Delivery_Kind] primary key clustered ([ID_Delivery_Kind] ASC) on [PRIMARY],
	constraint [UQ_Name_Delivery_Kind] unique ([Name_Delivery_Kind])
)
go

create table [dbo].[Categories]
(
	[ID_Category] [int] not null identity(1,1),
	[Name_Category] [varchar] (50) not null
	constraint [PK_Name_Category] primary key clustered ([ID_Category] ASC) on [PRIMARY],
	constraint [UQ_Name_Category] unique ([Name_Category])
)
go

create table [dbo].[Counterparties]
(
	[ID_Counterparty] [int] not null identity(1,1),
	[INN] [varchar] (12) not null
	constraint [PK_INN] primary key clustered ([ID_Counterparty] ASC) on [PRIMARY],
	constraint [UQ_INN] unique ([INN]),
	[Name] [varchar] (50) not null,
	[KPP] [varchar] (9),
	[Email] [varchar] (50) not null,
	[Contact_person] [varchar] (50) not null,
	[Number_phone] [varchar] (13) not null,
	[Checking_account] [varchar] (50) not null
)
go

create table [dbo].[Cards]
(
	[ID_Card] [int] not null identity(1,1),
	[Number_Card] [varchar] (19) not null
	constraint [PK_Number_Card] primary key clustered ([ID_Card] ASC) on [PRIMARY],
	constraint [UQ_Number_Card] unique ([Number_Card]),
	[Validity] [date] not null,
	[Holder] [varchar] (50) not null,
	[isArchived] [bit] default 0
)
go

create table [dbo].[Employees]
(
	[ID_Employee] [int] not null identity(1,1),
	[Login] [varchar] (50) not null,
	constraint [PK_Employee_ID] primary key clustered ([ID_Employee] ASC) on [PRIMARY],
	constraint [UQ_Login] unique ([Login]),
	[FIO] [varchar] (50) not null,
	[Birthdate] [date] null,
	[Phone] [varchar] (13) null,
	[Pasport] [varchar] (100) null,
	[Salt] [varchar] (255) not null,
	[Email] [varchar] (50) null
)
go

create table [dbo].[Deliveries]
(
	[ID_Delivery] [int] not null identity(1,1),
	constraint [PK_ID_Delivery] primary key clustered ([ID_Delivery] ASC) on [PRIMARY],
	constraint [UQ_ID_Delivery] unique ([ID_Delivery]),
	[Datetime] [datetime] not null,
	[Address] [varchar] (100) not null,
	[DeliveryKind_ID] [int] not null
	constraint [FK_Delivery_Kind] foreign key ([DeliveryKind_ID]) references [dbo].[Delivery_Kind] ([ID_Delivery_Kind]) ON UPDATE CASCADE,
	[DeliveryEmployee_ID] [int] not null
	constraint [FK_Delivery_Employee] foreign key ([DeliveryEmployee_ID]) references [dbo].[Employees] ([ID_Employee]) ON UPDATE CASCADE
)

create table [dbo].[Brands]
(
	[Id_Brand] [int] not null identity(1,1),
	[NameBrand] [varchar] (50) not null,
	constraint [PK_Name_Brand] primary key clustered ([Id_Brand]) on [PRIMARY],
	constraint [UQ_Name_Brand] unique ([NameBrand]),
)

create table [dbo].[Goods]
(
	[Vendor_code] [int] not null identity(1,1),
	constraint [PK_Vendor_code] primary key clustered ([Vendor_code] ASC) on [PRIMARY],
	constraint [UQ_Vendor_code] unique ([Vendor_code]),
	[Selling_Price] [float] not null,
	[Photo] [varchar] (MAX) not null,
	[Description] [varchar] (400) null,
	[Category_ID] [int] not null,
	[Name] [VARCHAR](50) NOT NULL,
	[Brand_ID] [int] NOT NULL,
	constraint [FK_Name_Category] foreign key ([Category_ID]) references [dbo].[Categories] ([ID_Category]) ON UPDATE CASCADE,
	constraint [FK_Name_Brand] foreign key ([Brand_ID]) references [dbo].[Brands] ([ID_Brand]) ON UPDATE CASCADE
)

create table [dbo].[Warehouses]
(
	[Id_Warehouses] [int] not null identity(1,1),
	[Name_Warehouse] [varchar] (50) not null,
	constraint [PK_Name_Warehouse] primary key clustered ([Id_Warehouses] ASC) on [PRIMARY],
	constraint [UQ_Name_Warehouse] unique ([Name_Warehouse])
)

create table [dbo].[Purchases]
(
	[ID_Purchase] [int] not null identity(1,1),
	constraint [PK_ID_Purchase] primary key clustered ([ID_Purchase] ASC) on [PRIMARY],
	constraint [UQ_ID_Purchase] unique ([ID_Purchase]), 
	[Count] [int] not null,
	[Purchase_price] [float] not null,
	[Date_purchase] [date] not null,
	[INN] [varchar] (12) not null
	constraint [FK_INN] foreign key ([INN]) references [dbo].[Counterparties] ([INN]) ON UPDATE CASCADE,
	[Warehouse_ID] [int] not null
	constraint [FK_Name_Warehouse] foreign key ([Warehouse_ID]) references [dbo].[Warehouses] ([ID_Warehouses]) ON UPDATE CASCADE,
	[Vendor_code] [int] not null
	constraint [FK_Vendor_code] foreign key ([Vendor_code]) references [dbo].[Goods] ([Vendor_code]) ON UPDATE CASCADE,
)

create table [dbo].[Clients]
(
	[ID_Client] [int] not null identity(1,1),
	[Login] [varchar] (50) not null,
	constraint [PK_Client_Login] primary key clustered ([ID_Client] ASC) on [PRIMARY],
	constraint [UQ_Client_Login] unique ([Login]),
	[FIO] [varchar] (50) not null,
	[Birthdate] [date] null,
	[Photo] [varchar] (max) null,
	[Phone] [varchar] (13) not null,
	[Salt] [varchar] (255) not null,
	[Email] [varchar] (50) null,
	[NumberCard_ID] [int] null
	constraint [FK_Number_Card] foreign key ([NumberCard_ID]) references [dbo].[Cards] ([ID_Card]) ON DELETE SET NULL,
	[Role_ID] [int] not null
	constraint [FK_Name_Role] foreign key ([Role_ID]) references [dbo].[Roles] ([ID_Role]) ON UPDATE CASCADE,
)

create table [dbo].[Posts]
(
	[ID_Post] [int] not null identity(1,1),
	[Name_post] [varchar] (50) not null,
	constraint [PK_Name_post] primary key clustered ([ID_Post] ASC) on [PRIMARY],
	constraint [UQ_Name_post] unique ([Name_post]),
	[Role_ID] [int] not null
	constraint [FK_Post_Role] foreign key ([Role_ID]) references [dbo].[Roles] ([ID_Role]) ON UPDATE CASCADE,
	[Employee_ID] [int] not null
	constraint [FK_Login_Employee] foreign key ([Employee_ID]) references [dbo].[Employees] ([ID_Employee]) ON UPDATE CASCADE,
)

create table [dbo].[Basket]
(
	[ID_Basket] [int] not null identity(1,1),
	[Count] [int] not null default 1,
	constraint [PK_ID_Basket] primary key clustered ([ID_Basket] ASC) on [PRIMARY],
	constraint [UQ_ID_Basket] unique ([ID_Basket]),
	constraint [UQ_BasketClient_ID] unique ([BasketClient_ID]),
	[BasketClient_ID] [int] not null
	constraint [FK_Basket_Client] foreign key ([BasketClient_ID]) references [dbo].[Clients] ([ID_Client]) ON UPDATE CASCADE,
	[Basket_Vendor_code] [int] not null
	constraint [FK_Basket_Vendor_code] foreign key ([Basket_Vendor_code]) references [dbo].[Goods] ([Vendor_code]) ON UPDATE CASCADE,
)

create table [dbo].[Orders]
(
	[ID_Order] [int] not null identity(1,1)
	constraint [PK_ID_Order] primary key clustered ([ID_Order] ASC) on [PRIMARY],
	constraint [UQ_ID_Order] unique ([ID_Order]),
	[Date] [datetime] not null default getDate(),
	[Status] [varchar] (50) default '� ���������',
	[OrderClient_ID] [int] not null
	constraint [FK_Order_Client] foreign key ([OrderClient_ID]) references [dbo].[Clients] ([ID_Client]) ,
	[OrderEmployee_ID] [int] not null
	constraint [FK_Order_Employee] foreign key ([OrderEmployee_ID]) references [dbo].[Employees] ([ID_Employee]),
	[OrderDelivery_ID] [int] not null
	constraint [FK_Order_Delivery] foreign key ([OrderDelivery_ID]) references [dbo].[Deliveries] ([ID_Delivery]),
)

create table [dbo].[Orders_Description]
(
	[ID_Order_Desc] [int] not null identity(1,1)
	constraint [PK_ID_Order_Desc] primary key clustered ([ID_Order_Desc] ASC) on [PRIMARY],
	constraint [UQ_ID_Order_Desc] unique ([ID_Order_Desc]),
	[ID_Order] [int] not null
	constraint [FK_Order_Desc] foreign key ([ID_Order]) references [dbo].[Orders] ([ID_Order]) ON UPDATE CASCADE,
	[Count] [int] not null,
	[Vendor_code] [int] not null
	constraint [FK_Order_Vendor_code] foreign key (Vendor_code) references [dbo].[Goods] ([Vendor_code]) ON UPDATE CASCADE
)